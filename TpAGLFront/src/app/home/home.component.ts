import { Component, OnInit } from '@angular/core';
import {UsersService} from '../services/users.service';
import {User} from '../model/user';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  enseignants: Array<User>;
  term: string ;
  itemsPerPage: number = 6;
  page: number = 1;

  constructor(private usersService:UsersService) { }

  ngOnInit(): void {
    this.usersService.getAllEnseignants();
    this.usersService.enseignantsSubject.asObservable().subscribe((profs:[User])=>{
      this.enseignants=profs
    })


  }

  onAffichePlus() {
    this.itemsPerPage=this.enseignants?.length
  }

  onAfficheMoins() {
    this.itemsPerPage=6
  }
}
