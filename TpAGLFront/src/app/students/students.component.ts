import { Component, OnInit } from '@angular/core';
import {User} from '../model/user';
import {UsersService} from '../services/users.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students: Array<User>;
  term: string ;
  itemsPerPage: number = 6;
  page: number = 1;

  constructor(private usersService:UsersService) { }

  ngOnInit(): void {
    this.usersService.getAllStudents();
    this.usersService.studentsSubject.asObservable().subscribe((students:[User])=>{
      this.students=students
    })
  }

  onAffichePlus() {
    this.itemsPerPage=this.students?.length
  }

  onAfficheMoins() {
    this.itemsPerPage=6
  }

}
