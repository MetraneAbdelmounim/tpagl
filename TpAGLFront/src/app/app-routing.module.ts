import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SignUpComponent} from './sign-up/sign-up.component';
import {HomeComponent} from './home/home.component';
import {StudentsComponent} from './students/students.component';



const routes: Routes = [
  {path: 'sign-up',component:SignUpComponent},
  {path: 'enseignants',component:HomeComponent},
  {path: 'etudiants',component:StudentsComponent},
  {path: '',component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
