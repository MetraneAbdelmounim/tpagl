import { Injectable } from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from '../model/user';
import {Subject} from 'rxjs';
const BACKEND_URL = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  enseignants: Array<User>;
  enseignantsSubject = new Subject<Array<User>>();
  studentsSubject=new Subject<Array<User>>();
  students:Array<User>;
  constructor(private http:HttpClient) { }

  signup(f: NgForm,image:File) {
    let memberBody = new FormData();
    memberBody.append('user', JSON.stringify(f.value));
    memberBody.append('image', image);
    return this.http.post(BACKEND_URL+"users/sign-up",memberBody);
  }

  getAllEnseignants() {
    this.http.get(BACKEND_URL + "users/enseignants").subscribe((result:any)=>{

      this.enseignants=result
      this.emitEnseignatsSubject()
    })
  }
  emitEnseignatsSubject() {
    this.enseignantsSubject.next(this.enseignants.slice())
  }
  getAllStudents() {
    this.http.get(BACKEND_URL + "users/students").subscribe((result:any)=>{

      this.students=result
      this.emitStudentsSubject()
    })
  }
  emitStudentsSubject() {
    this.studentsSubject.next(this.students.slice())
  }
}
