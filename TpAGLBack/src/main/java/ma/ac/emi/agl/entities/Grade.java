package ma.ac.emi.agl.entities;

public enum Grade {
	Professeur,
	Doctorant,
	Ingénieur
}
