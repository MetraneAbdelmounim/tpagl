package ma.ac.emi.agl.metier;

import java.util.List;
import java.util.Optional;

import ma.ac.emi.agl.entities.Enseignant;
import ma.ac.emi.agl.entities.Etudiant;
import ma.ac.emi.agl.entities.Utilisateur;


public interface UtilisateurMetier {
	public Utilisateur register(Utilisateur user);
	public Optional<Utilisateur> findUserByEmail(String email);

	public List<Utilisateur> getAllEnseignants();
	public List<Utilisateur> getAllStudents();
}
