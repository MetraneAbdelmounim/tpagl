package ma.ac.emi.agl.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.ac.emi.agl.entities.Enseignant;
import ma.ac.emi.agl.entities.Role;
import ma.ac.emi.agl.entities.Utilisateur;



public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long>{
	Optional<Utilisateur> findByEmail(String email);
	
	List<Utilisateur> findByRole(Role role);
	
	
}
