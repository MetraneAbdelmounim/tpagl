package ma.ac.emi.agl.entities;

public enum Role {
	Enseignant,
	Admin,
	Etudiant
}
