package ma.ac.emi.agl.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ma.ac.emi.agl.entities.Enseignant;
import ma.ac.emi.agl.entities.Etudiant;
import ma.ac.emi.agl.entities.Role;
import ma.ac.emi.agl.entities.Utilisateur;
import ma.ac.emi.agl.metier.StorageFileMetier;
import ma.ac.emi.agl.metier.UtilisateurMetier;
import ma.ac.emi.agl.request.UserRequest;
import ma.ec.emi.config.Constantes;



@RestController
@RequestMapping("users")
public class UserController {
	
	@Autowired
	private UtilisateurMetier utilisateurMetier;
	
	@Autowired
	private StorageFileMetier storageFileMetier;

	
	@PostMapping("/sign-up")
	public Utilisateur register(@RequestParam("user") String userData,@RequestParam(value="image",required = false) MultipartFile file) throws Exception {
		UserRequest userReq = new ObjectMapper().readValue(userData, UserRequest.class) ;
		if(this.utilisateurMetier.findUserByEmail(userReq.getEmail()).isPresent()) {
			throw new Exception("Cet Email est déja pris ! essayer avec un autre . ");
		}
		Utilisateur user=null;
		if(userReq.getRole().equals(Role.Enseignant)) {
			user = new Enseignant(userReq.getNom(), userReq.getPrenom(), userReq.getEmail(),userReq.getPassword(), userReq.getPhotoProfile(), userReq.getGrade());
			String imageName = this.storageFileMetier.store(file);
			user.setPhotoProfile(Constantes.uri+"upload-dir/"+imageName);		
		}
		else if(userReq.getRole().equals(Role.Etudiant)) {
			user = new Etudiant(userReq.getNom(), userReq.getPrenom(), userReq.getEmail(),userReq.getPassword(), userReq.getPhotoProfile());
			String imageName = this.storageFileMetier.store(file);
			user.setPhotoProfile(Constantes.uri+"upload-dir/"+imageName);			
		}
		
		return  this.utilisateurMetier.register(user);
	}
	@GetMapping("/enseignants")
	public List<Utilisateur>  getEnseignants() {
		return this.utilisateurMetier.getAllEnseignants();
	}
	
	@GetMapping("/students")
	public List<Utilisateur>  getStudents() {
		return this.utilisateurMetier.getAllStudents();
	}
	
}

