package ma.ac.emi.agl.metier;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.ac.emi.agl.entities.Enseignant;
import ma.ac.emi.agl.entities.Etudiant;
import ma.ac.emi.agl.entities.Role;
import ma.ac.emi.agl.entities.Utilisateur;
import ma.ac.emi.agl.repositories.UtilisateurRepository;

@Service
public class UtilisateurMetierImpl implements UtilisateurMetier{
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Override
	public Utilisateur register(Utilisateur user) {
		// TODO Auto-generated method stub
		return this.utilisateurRepository.save(user);
	}

	@Override
	public Optional<Utilisateur> findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return this.utilisateurRepository.findByEmail(email);
	}

	@Override
	public List<Utilisateur> getAllEnseignants() {
		// TODO Auto-generated method stub
		return this.utilisateurRepository.findByRole(Role.Enseignant);
	}

	@Override
	public List<Utilisateur> getAllStudents() {
		// TODO Auto-generated method stub
		return  this.utilisateurRepository.findByRole(Role.Etudiant);
	}


}
